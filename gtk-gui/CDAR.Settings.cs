
// This file has been generated by the GUI designer. Do not modify.
namespace CDAR
{
	public partial class Settings
	{
		private global::Gtk.VBox vbox1;

		private global::Gtk.Notebook notebook1;

		private global::Gtk.ScrolledWindow scrolledwindow1;

		private global::Gtk.Table table1;

		private global::Gtk.Label label12;

		private global::Gtk.Label label2;

		private global::Gtk.Label label3;

		private global::Gtk.Label label4;

		private global::Gtk.Label label5;

		private global::Gtk.Label label6;

		private global::Gtk.Label label7;

		private global::Gtk.ComboBox ripChecking;

		private global::Gtk.Entry ripDevice;

		private global::Gtk.CheckButton ripEject;

		private global::Gtk.Entry ripFilename;

		private global::Gtk.Entry ripMultiDirectory;

		private global::Gtk.Button ripMultiDirectoryOpen;

		private global::Gtk.Entry ripSingleDirectory;

		private global::Gtk.Button ripSingleDirectoryOpen;

		private global::Gtk.SpinButton ripSpeed;

		private global::Gtk.Label label1;

		private global::Gtk.ScrolledWindow scrolledwindow2;

		private global::Gtk.Table table2;

		private global::Gtk.CheckButton convertAac;

		private global::Gtk.Label convertAacLabel;

		private global::Gtk.HScale convertAacQuality;

		private global::Gtk.CheckButton convertFlac;

		private global::Gtk.HScale convertFlacCompress;

		private global::Gtk.Label convertFlacLabel;

		private global::Gtk.CheckButton convertMp3;

		private global::Gtk.Label convertMp3Label;

		private global::Gtk.HScale convertMp3Quality;

		private global::Gtk.CheckButton convertVorbis;

		private global::Gtk.Label convertVorbisLabel;

		private global::Gtk.HScale convertVorbisQuality;

		private global::Gtk.CheckButton convertWav;

		private global::Gtk.Label label8;

		private global::Gtk.ScrolledWindow scrolledwindow3;

		private global::Gtk.Table table3;

		private global::Gtk.HSeparator hseparator1;

		private global::Gtk.Label label10;

		private global::Gtk.Label label11;

		private global::Gtk.CheckButton otherMusicBrainz;

		private global::Gtk.CheckButton otherPlaylist;

		private global::Gtk.Entry otherPlaylistDirectory;

		private global::Gtk.Button otherPlaylistDirectoryOpen;

		private global::Gtk.Entry otherPlaylistFilename;

		private global::Gtk.CheckButton otherPlaylistRelative;

		private global::Gtk.Label label9;

		private global::Gtk.HButtonBox hbuttonbox1;

		private global::Gtk.Button button1;

		private global::Gtk.Button button2;

		private global::Gtk.Button button3;

		protected virtual void Build ()
		{
			global::Stetic.Gui.Initialize(this);
			// Widget CDAR.Settings
			this.Name = "CDAR.Settings";
			this.Title = global::Mono.Unix.Catalog.GetString("Settings");
			this.WindowPosition = ((global::Gtk.WindowPosition)(4));
			this.DefaultWidth = 400;
			this.DefaultHeight = 300;
			// Container child CDAR.Settings.Gtk.Container+ContainerChild
			this.vbox1 = new global::Gtk.VBox();
			this.vbox1.Name = "vbox1";
			// Container child vbox1.Gtk.Box+BoxChild
			this.notebook1 = new global::Gtk.Notebook();
			this.notebook1.CanFocus = true;
			this.notebook1.Name = "notebook1";
			this.notebook1.CurrentPage = 0;
			// Container child notebook1.Gtk.Notebook+NotebookChild
			this.scrolledwindow1 = new global::Gtk.ScrolledWindow();
			this.scrolledwindow1.CanFocus = true;
			this.scrolledwindow1.Name = "scrolledwindow1";
			this.scrolledwindow1.HscrollbarPolicy = ((global::Gtk.PolicyType)(2));
			// Container child scrolledwindow1.Gtk.Container+ContainerChild
			global::Gtk.Viewport w1 = new global::Gtk.Viewport();
			w1.ShadowType = ((global::Gtk.ShadowType)(0));
			// Container child GtkViewport.Gtk.Container+ContainerChild
			this.table1 = new global::Gtk.Table(((uint)(8)), ((uint)(3)), false);
			this.table1.Name = "table1";
			this.table1.RowSpacing = ((uint)(2));
			this.table1.ColumnSpacing = ((uint)(2));
			// Container child table1.Gtk.Table+TableChild
			this.label12 = new global::Gtk.Label();
			this.label12.Name = "label12";
			this.label12.Xalign = 0F;
			this.label12.LabelProp = global::Mono.Unix.Catalog.GetString("Directory for multiple CD");
			this.table1.Add(this.label12);
			global::Gtk.Table.TableChild w2 = ((global::Gtk.Table.TableChild)(this.table1[this.label12]));
			w2.TopAttach = ((uint)(5));
			w2.BottomAttach = ((uint)(6));
			w2.XPadding = ((uint)(3));
			w2.XOptions = ((global::Gtk.AttachOptions)(4));
			w2.YOptions = ((global::Gtk.AttachOptions)(4));
			// Container child table1.Gtk.Table+TableChild
			this.label2 = new global::Gtk.Label();
			this.label2.Name = "label2";
			this.label2.Xalign = 0F;
			this.label2.LabelProp = global::Mono.Unix.Catalog.GetString("Device");
			this.table1.Add(this.label2);
			global::Gtk.Table.TableChild w3 = ((global::Gtk.Table.TableChild)(this.table1[this.label2]));
			w3.XPadding = ((uint)(3));
			w3.XOptions = ((global::Gtk.AttachOptions)(4));
			w3.YOptions = ((global::Gtk.AttachOptions)(4));
			// Container child table1.Gtk.Table+TableChild
			this.label3 = new global::Gtk.Label();
			this.label3.Name = "label3";
			this.label3.Xalign = 0F;
			this.label3.LabelProp = global::Mono.Unix.Catalog.GetString("Speed");
			this.table1.Add(this.label3);
			global::Gtk.Table.TableChild w4 = ((global::Gtk.Table.TableChild)(this.table1[this.label3]));
			w4.TopAttach = ((uint)(1));
			w4.BottomAttach = ((uint)(2));
			w4.XPadding = ((uint)(3));
			w4.XOptions = ((global::Gtk.AttachOptions)(4));
			w4.YOptions = ((global::Gtk.AttachOptions)(4));
			// Container child table1.Gtk.Table+TableChild
			this.label4 = new global::Gtk.Label();
			this.label4.Name = "label4";
			this.label4.Xalign = 0F;
			this.label4.LabelProp = global::Mono.Unix.Catalog.GetString("Checking");
			this.table1.Add(this.label4);
			global::Gtk.Table.TableChild w5 = ((global::Gtk.Table.TableChild)(this.table1[this.label4]));
			w5.TopAttach = ((uint)(2));
			w5.BottomAttach = ((uint)(3));
			w5.XPadding = ((uint)(3));
			w5.XOptions = ((global::Gtk.AttachOptions)(4));
			w5.YOptions = ((global::Gtk.AttachOptions)(4));
			// Container child table1.Gtk.Table+TableChild
			this.label5 = new global::Gtk.Label();
			this.label5.Name = "label5";
			this.label5.Xalign = 0F;
			this.label5.LabelProp = global::Mono.Unix.Catalog.GetString("Filename");
			this.table1.Add(this.label5);
			global::Gtk.Table.TableChild w6 = ((global::Gtk.Table.TableChild)(this.table1[this.label5]));
			w6.TopAttach = ((uint)(6));
			w6.BottomAttach = ((uint)(7));
			w6.XPadding = ((uint)(3));
			w6.XOptions = ((global::Gtk.AttachOptions)(4));
			w6.YOptions = ((global::Gtk.AttachOptions)(4));
			// Container child table1.Gtk.Table+TableChild
			this.label6 = new global::Gtk.Label();
			this.label6.Name = "label6";
			this.label6.LabelProp = global::Mono.Unix.Catalog.GetString("<b>Accepted variables:</b>\n<i>%track%</i> - track number\n<i>%title%</i> - track n" +
					"ame\n<i>%album%</i> - album title\n<i>%artist%</i> - album artist\n<i>%year%</i> - " +
					"album year\n<i>%genre%</i> - album genre\n<i>%disc%</i> - disc number");
			this.label6.UseMarkup = true;
			this.label6.Wrap = true;
			this.label6.Justify = ((global::Gtk.Justification)(2));
			this.table1.Add(this.label6);
			global::Gtk.Table.TableChild w7 = ((global::Gtk.Table.TableChild)(this.table1[this.label6]));
			w7.TopAttach = ((uint)(7));
			w7.BottomAttach = ((uint)(8));
			w7.LeftAttach = ((uint)(1));
			w7.RightAttach = ((uint)(3));
			w7.XOptions = ((global::Gtk.AttachOptions)(4));
			w7.YOptions = ((global::Gtk.AttachOptions)(4));
			// Container child table1.Gtk.Table+TableChild
			this.label7 = new global::Gtk.Label();
			this.label7.Name = "label7";
			this.label7.Xalign = 0F;
			this.label7.LabelProp = global::Mono.Unix.Catalog.GetString("Directory for single CD");
			this.table1.Add(this.label7);
			global::Gtk.Table.TableChild w8 = ((global::Gtk.Table.TableChild)(this.table1[this.label7]));
			w8.TopAttach = ((uint)(4));
			w8.BottomAttach = ((uint)(5));
			w8.XPadding = ((uint)(3));
			w8.XOptions = ((global::Gtk.AttachOptions)(4));
			w8.YOptions = ((global::Gtk.AttachOptions)(4));
			// Container child table1.Gtk.Table+TableChild
			this.ripChecking = global::Gtk.ComboBox.NewText();
			this.ripChecking.AppendText(global::Mono.Unix.Catalog.GetString("Full"));
			this.ripChecking.AppendText(global::Mono.Unix.Catalog.GetString("Limited"));
			this.ripChecking.AppendText(global::Mono.Unix.Catalog.GetString("None"));
			this.ripChecking.Name = "ripChecking";
			this.ripChecking.Active = 0;
			this.table1.Add(this.ripChecking);
			global::Gtk.Table.TableChild w9 = ((global::Gtk.Table.TableChild)(this.table1[this.ripChecking]));
			w9.TopAttach = ((uint)(2));
			w9.BottomAttach = ((uint)(3));
			w9.LeftAttach = ((uint)(1));
			w9.RightAttach = ((uint)(3));
			w9.XOptions = ((global::Gtk.AttachOptions)(4));
			w9.YOptions = ((global::Gtk.AttachOptions)(4));
			// Container child table1.Gtk.Table+TableChild
			this.ripDevice = new global::Gtk.Entry();
			this.ripDevice.CanFocus = true;
			this.ripDevice.Name = "ripDevice";
			this.ripDevice.IsEditable = true;
			this.ripDevice.InvisibleChar = '•';
			this.table1.Add(this.ripDevice);
			global::Gtk.Table.TableChild w10 = ((global::Gtk.Table.TableChild)(this.table1[this.ripDevice]));
			w10.LeftAttach = ((uint)(1));
			w10.RightAttach = ((uint)(3));
			w10.YOptions = ((global::Gtk.AttachOptions)(4));
			// Container child table1.Gtk.Table+TableChild
			this.ripEject = new global::Gtk.CheckButton();
			this.ripEject.CanFocus = true;
			this.ripEject.Name = "ripEject";
			this.ripEject.Label = global::Mono.Unix.Catalog.GetString("Eject after complete");
			this.ripEject.DrawIndicator = true;
			this.ripEject.UseUnderline = true;
			this.table1.Add(this.ripEject);
			global::Gtk.Table.TableChild w11 = ((global::Gtk.Table.TableChild)(this.table1[this.ripEject]));
			w11.TopAttach = ((uint)(3));
			w11.BottomAttach = ((uint)(4));
			w11.LeftAttach = ((uint)(1));
			w11.RightAttach = ((uint)(3));
			w11.XOptions = ((global::Gtk.AttachOptions)(4));
			w11.YOptions = ((global::Gtk.AttachOptions)(4));
			// Container child table1.Gtk.Table+TableChild
			this.ripFilename = new global::Gtk.Entry();
			this.ripFilename.CanFocus = true;
			this.ripFilename.Name = "ripFilename";
			this.ripFilename.IsEditable = true;
			this.ripFilename.InvisibleChar = '•';
			this.table1.Add(this.ripFilename);
			global::Gtk.Table.TableChild w12 = ((global::Gtk.Table.TableChild)(this.table1[this.ripFilename]));
			w12.TopAttach = ((uint)(6));
			w12.BottomAttach = ((uint)(7));
			w12.LeftAttach = ((uint)(1));
			w12.RightAttach = ((uint)(3));
			w12.XOptions = ((global::Gtk.AttachOptions)(4));
			w12.YOptions = ((global::Gtk.AttachOptions)(4));
			// Container child table1.Gtk.Table+TableChild
			this.ripMultiDirectory = new global::Gtk.Entry();
			this.ripMultiDirectory.CanFocus = true;
			this.ripMultiDirectory.Name = "ripMultiDirectory";
			this.ripMultiDirectory.IsEditable = true;
			this.ripMultiDirectory.InvisibleChar = '•';
			this.table1.Add(this.ripMultiDirectory);
			global::Gtk.Table.TableChild w13 = ((global::Gtk.Table.TableChild)(this.table1[this.ripMultiDirectory]));
			w13.TopAttach = ((uint)(5));
			w13.BottomAttach = ((uint)(6));
			w13.LeftAttach = ((uint)(1));
			w13.RightAttach = ((uint)(2));
			w13.XOptions = ((global::Gtk.AttachOptions)(4));
			w13.YOptions = ((global::Gtk.AttachOptions)(4));
			// Container child table1.Gtk.Table+TableChild
			this.ripMultiDirectoryOpen = new global::Gtk.Button();
			this.ripMultiDirectoryOpen.CanFocus = true;
			this.ripMultiDirectoryOpen.Name = "ripMultiDirectoryOpen";
			this.table1.Add(this.ripMultiDirectoryOpen);
			global::Gtk.Table.TableChild w14 = ((global::Gtk.Table.TableChild)(this.table1[this.ripMultiDirectoryOpen]));
			w14.TopAttach = ((uint)(5));
			w14.BottomAttach = ((uint)(6));
			w14.LeftAttach = ((uint)(2));
			w14.RightAttach = ((uint)(3));
			w14.XOptions = ((global::Gtk.AttachOptions)(0));
			w14.YOptions = ((global::Gtk.AttachOptions)(0));
			// Container child table1.Gtk.Table+TableChild
			this.ripSingleDirectory = new global::Gtk.Entry();
			this.ripSingleDirectory.CanFocus = true;
			this.ripSingleDirectory.Name = "ripSingleDirectory";
			this.ripSingleDirectory.IsEditable = true;
			this.ripSingleDirectory.InvisibleChar = '•';
			this.table1.Add(this.ripSingleDirectory);
			global::Gtk.Table.TableChild w15 = ((global::Gtk.Table.TableChild)(this.table1[this.ripSingleDirectory]));
			w15.TopAttach = ((uint)(4));
			w15.BottomAttach = ((uint)(5));
			w15.LeftAttach = ((uint)(1));
			w15.RightAttach = ((uint)(2));
			w15.YOptions = ((global::Gtk.AttachOptions)(4));
			// Container child table1.Gtk.Table+TableChild
			this.ripSingleDirectoryOpen = new global::Gtk.Button();
			this.ripSingleDirectoryOpen.CanFocus = true;
			this.ripSingleDirectoryOpen.Name = "ripSingleDirectoryOpen";
			this.table1.Add(this.ripSingleDirectoryOpen);
			global::Gtk.Table.TableChild w16 = ((global::Gtk.Table.TableChild)(this.table1[this.ripSingleDirectoryOpen]));
			w16.TopAttach = ((uint)(4));
			w16.BottomAttach = ((uint)(5));
			w16.LeftAttach = ((uint)(2));
			w16.RightAttach = ((uint)(3));
			w16.XOptions = ((global::Gtk.AttachOptions)(0));
			w16.YOptions = ((global::Gtk.AttachOptions)(0));
			// Container child table1.Gtk.Table+TableChild
			this.ripSpeed = new global::Gtk.SpinButton(0D, 72D, 1D);
			this.ripSpeed.CanFocus = true;
			this.ripSpeed.Name = "ripSpeed";
			this.ripSpeed.Adjustment.PageIncrement = 10D;
			this.ripSpeed.ClimbRate = 1D;
			this.ripSpeed.Numeric = true;
			this.table1.Add(this.ripSpeed);
			global::Gtk.Table.TableChild w17 = ((global::Gtk.Table.TableChild)(this.table1[this.ripSpeed]));
			w17.TopAttach = ((uint)(1));
			w17.BottomAttach = ((uint)(2));
			w17.LeftAttach = ((uint)(1));
			w17.RightAttach = ((uint)(3));
			w17.XOptions = ((global::Gtk.AttachOptions)(4));
			w17.YOptions = ((global::Gtk.AttachOptions)(4));
			w1.Add(this.table1);
			this.scrolledwindow1.Add(w1);
			this.notebook1.Add(this.scrolledwindow1);
			// Notebook tab
			this.label1 = new global::Gtk.Label();
			this.label1.Name = "label1";
			this.label1.LabelProp = global::Mono.Unix.Catalog.GetString("Rip");
			this.notebook1.SetTabLabel(this.scrolledwindow1, this.label1);
			this.label1.ShowAll();
			// Container child notebook1.Gtk.Notebook+NotebookChild
			this.scrolledwindow2 = new global::Gtk.ScrolledWindow();
			this.scrolledwindow2.CanFocus = true;
			this.scrolledwindow2.Name = "scrolledwindow2";
			this.scrolledwindow2.HscrollbarPolicy = ((global::Gtk.PolicyType)(2));
			// Container child scrolledwindow2.Gtk.Container+ContainerChild
			global::Gtk.Viewport w21 = new global::Gtk.Viewport();
			w21.ShadowType = ((global::Gtk.ShadowType)(0));
			// Container child GtkViewport1.Gtk.Container+ContainerChild
			this.table2 = new global::Gtk.Table(((uint)(9)), ((uint)(2)), false);
			this.table2.Name = "table2";
			this.table2.RowSpacing = ((uint)(2));
			this.table2.ColumnSpacing = ((uint)(2));
			// Container child table2.Gtk.Table+TableChild
			this.convertAac = new global::Gtk.CheckButton();
			this.convertAac.CanFocus = true;
			this.convertAac.Name = "convertAac";
			this.convertAac.Label = global::Mono.Unix.Catalog.GetString("AAC (in .m4a)");
			this.convertAac.DrawIndicator = true;
			this.convertAac.UseUnderline = true;
			this.table2.Add(this.convertAac);
			global::Gtk.Table.TableChild w22 = ((global::Gtk.Table.TableChild)(this.table2[this.convertAac]));
			w22.TopAttach = ((uint)(7));
			w22.BottomAttach = ((uint)(8));
			w22.RightAttach = ((uint)(2));
			w22.YOptions = ((global::Gtk.AttachOptions)(4));
			// Container child table2.Gtk.Table+TableChild
			this.convertAacLabel = new global::Gtk.Label();
			this.convertAacLabel.Name = "convertAacLabel";
			this.convertAacLabel.Xalign = 0F;
			this.convertAacLabel.LabelProp = global::Mono.Unix.Catalog.GetString("\tQuality");
			this.table2.Add(this.convertAacLabel);
			global::Gtk.Table.TableChild w23 = ((global::Gtk.Table.TableChild)(this.table2[this.convertAacLabel]));
			w23.TopAttach = ((uint)(8));
			w23.BottomAttach = ((uint)(9));
			w23.XPadding = ((uint)(3));
			w23.XOptions = ((global::Gtk.AttachOptions)(4));
			w23.YOptions = ((global::Gtk.AttachOptions)(4));
			// Container child table2.Gtk.Table+TableChild
			this.convertAacQuality = new global::Gtk.HScale(null);
			this.convertAacQuality.CanFocus = true;
			this.convertAacQuality.Name = "convertAacQuality";
			this.convertAacQuality.Adjustment.Lower = 0.1D;
			this.convertAacQuality.Adjustment.Upper = 5D;
			this.convertAacQuality.Adjustment.PageIncrement = 1D;
			this.convertAacQuality.Adjustment.StepIncrement = 0.1D;
			this.convertAacQuality.Adjustment.Value = 1D;
			this.convertAacQuality.DrawValue = true;
			this.convertAacQuality.Digits = 0;
			this.convertAacQuality.ValuePos = ((global::Gtk.PositionType)(1));
			this.table2.Add(this.convertAacQuality);
			global::Gtk.Table.TableChild w24 = ((global::Gtk.Table.TableChild)(this.table2[this.convertAacQuality]));
			w24.TopAttach = ((uint)(8));
			w24.BottomAttach = ((uint)(9));
			w24.LeftAttach = ((uint)(1));
			w24.RightAttach = ((uint)(2));
			w24.XOptions = ((global::Gtk.AttachOptions)(4));
			w24.YOptions = ((global::Gtk.AttachOptions)(4));
			// Container child table2.Gtk.Table+TableChild
			this.convertFlac = new global::Gtk.CheckButton();
			this.convertFlac.CanFocus = true;
			this.convertFlac.Name = "convertFlac";
			this.convertFlac.Label = global::Mono.Unix.Catalog.GetString("FLAC");
			this.convertFlac.DrawIndicator = true;
			this.convertFlac.UseUnderline = true;
			this.table2.Add(this.convertFlac);
			global::Gtk.Table.TableChild w25 = ((global::Gtk.Table.TableChild)(this.table2[this.convertFlac]));
			w25.TopAttach = ((uint)(1));
			w25.BottomAttach = ((uint)(2));
			w25.RightAttach = ((uint)(2));
			w25.YOptions = ((global::Gtk.AttachOptions)(4));
			// Container child table2.Gtk.Table+TableChild
			this.convertFlacCompress = new global::Gtk.HScale(null);
			this.convertFlacCompress.CanFocus = true;
			this.convertFlacCompress.Name = "convertFlacCompress";
			this.convertFlacCompress.Adjustment.Upper = 8D;
			this.convertFlacCompress.Adjustment.PageIncrement = 4D;
			this.convertFlacCompress.Adjustment.StepIncrement = 1D;
			this.convertFlacCompress.Adjustment.Value = 5D;
			this.convertFlacCompress.DrawValue = true;
			this.convertFlacCompress.Digits = 0;
			this.convertFlacCompress.ValuePos = ((global::Gtk.PositionType)(1));
			this.table2.Add(this.convertFlacCompress);
			global::Gtk.Table.TableChild w26 = ((global::Gtk.Table.TableChild)(this.table2[this.convertFlacCompress]));
			w26.TopAttach = ((uint)(2));
			w26.BottomAttach = ((uint)(3));
			w26.LeftAttach = ((uint)(1));
			w26.RightAttach = ((uint)(2));
			w26.YOptions = ((global::Gtk.AttachOptions)(4));
			// Container child table2.Gtk.Table+TableChild
			this.convertFlacLabel = new global::Gtk.Label();
			this.convertFlacLabel.Name = "convertFlacLabel";
			this.convertFlacLabel.Xalign = 0F;
			this.convertFlacLabel.LabelProp = global::Mono.Unix.Catalog.GetString("\tCompression");
			this.table2.Add(this.convertFlacLabel);
			global::Gtk.Table.TableChild w27 = ((global::Gtk.Table.TableChild)(this.table2[this.convertFlacLabel]));
			w27.TopAttach = ((uint)(2));
			w27.BottomAttach = ((uint)(3));
			w27.XPadding = ((uint)(3));
			w27.XOptions = ((global::Gtk.AttachOptions)(4));
			w27.YOptions = ((global::Gtk.AttachOptions)(4));
			// Container child table2.Gtk.Table+TableChild
			this.convertMp3 = new global::Gtk.CheckButton();
			this.convertMp3.CanFocus = true;
			this.convertMp3.Name = "convertMp3";
			this.convertMp3.Label = global::Mono.Unix.Catalog.GetString("MP3");
			this.convertMp3.DrawIndicator = true;
			this.convertMp3.UseUnderline = true;
			this.table2.Add(this.convertMp3);
			global::Gtk.Table.TableChild w28 = ((global::Gtk.Table.TableChild)(this.table2[this.convertMp3]));
			w28.TopAttach = ((uint)(5));
			w28.BottomAttach = ((uint)(6));
			w28.RightAttach = ((uint)(2));
			w28.YOptions = ((global::Gtk.AttachOptions)(4));
			// Container child table2.Gtk.Table+TableChild
			this.convertMp3Label = new global::Gtk.Label();
			this.convertMp3Label.Name = "convertMp3Label";
			this.convertMp3Label.Xalign = 0F;
			this.convertMp3Label.LabelProp = global::Mono.Unix.Catalog.GetString("\tQuality");
			this.table2.Add(this.convertMp3Label);
			global::Gtk.Table.TableChild w29 = ((global::Gtk.Table.TableChild)(this.table2[this.convertMp3Label]));
			w29.TopAttach = ((uint)(6));
			w29.BottomAttach = ((uint)(7));
			w29.XPadding = ((uint)(3));
			w29.XOptions = ((global::Gtk.AttachOptions)(4));
			w29.YOptions = ((global::Gtk.AttachOptions)(4));
			// Container child table2.Gtk.Table+TableChild
			this.convertMp3Quality = new global::Gtk.HScale(null);
			this.convertMp3Quality.CanFocus = true;
			this.convertMp3Quality.Name = "convertMp3Quality";
			this.convertMp3Quality.Inverted = true;
			this.convertMp3Quality.Adjustment.Upper = 9D;
			this.convertMp3Quality.Adjustment.PageIncrement = 4D;
			this.convertMp3Quality.Adjustment.StepIncrement = 1D;
			this.convertMp3Quality.Adjustment.Value = 4D;
			this.convertMp3Quality.DrawValue = true;
			this.convertMp3Quality.Digits = 0;
			this.convertMp3Quality.ValuePos = ((global::Gtk.PositionType)(1));
			this.table2.Add(this.convertMp3Quality);
			global::Gtk.Table.TableChild w30 = ((global::Gtk.Table.TableChild)(this.table2[this.convertMp3Quality]));
			w30.TopAttach = ((uint)(6));
			w30.BottomAttach = ((uint)(7));
			w30.LeftAttach = ((uint)(1));
			w30.RightAttach = ((uint)(2));
			w30.YOptions = ((global::Gtk.AttachOptions)(4));
			// Container child table2.Gtk.Table+TableChild
			this.convertVorbis = new global::Gtk.CheckButton();
			this.convertVorbis.CanFocus = true;
			this.convertVorbis.Name = "convertVorbis";
			this.convertVorbis.Label = global::Mono.Unix.Catalog.GetString("OGG (Vorbis)");
			this.convertVorbis.DrawIndicator = true;
			this.convertVorbis.UseUnderline = true;
			this.table2.Add(this.convertVorbis);
			global::Gtk.Table.TableChild w31 = ((global::Gtk.Table.TableChild)(this.table2[this.convertVorbis]));
			w31.TopAttach = ((uint)(3));
			w31.BottomAttach = ((uint)(4));
			w31.RightAttach = ((uint)(2));
			w31.YOptions = ((global::Gtk.AttachOptions)(4));
			// Container child table2.Gtk.Table+TableChild
			this.convertVorbisLabel = new global::Gtk.Label();
			this.convertVorbisLabel.Name = "convertVorbisLabel";
			this.convertVorbisLabel.Xalign = 0F;
			this.convertVorbisLabel.LabelProp = global::Mono.Unix.Catalog.GetString("\tQuality");
			this.table2.Add(this.convertVorbisLabel);
			global::Gtk.Table.TableChild w32 = ((global::Gtk.Table.TableChild)(this.table2[this.convertVorbisLabel]));
			w32.TopAttach = ((uint)(4));
			w32.BottomAttach = ((uint)(5));
			w32.XPadding = ((uint)(3));
			w32.XOptions = ((global::Gtk.AttachOptions)(4));
			w32.YOptions = ((global::Gtk.AttachOptions)(4));
			// Container child table2.Gtk.Table+TableChild
			this.convertVorbisQuality = new global::Gtk.HScale(null);
			this.convertVorbisQuality.CanFocus = true;
			this.convertVorbisQuality.Name = "convertVorbisQuality";
			this.convertVorbisQuality.Adjustment.Lower = -1D;
			this.convertVorbisQuality.Adjustment.Upper = 10D;
			this.convertVorbisQuality.Adjustment.PageIncrement = 5D;
			this.convertVorbisQuality.Adjustment.StepIncrement = 1D;
			this.convertVorbisQuality.Adjustment.Value = 3D;
			this.convertVorbisQuality.DrawValue = true;
			this.convertVorbisQuality.Digits = 0;
			this.convertVorbisQuality.ValuePos = ((global::Gtk.PositionType)(1));
			this.table2.Add(this.convertVorbisQuality);
			global::Gtk.Table.TableChild w33 = ((global::Gtk.Table.TableChild)(this.table2[this.convertVorbisQuality]));
			w33.TopAttach = ((uint)(4));
			w33.BottomAttach = ((uint)(5));
			w33.LeftAttach = ((uint)(1));
			w33.RightAttach = ((uint)(2));
			w33.YOptions = ((global::Gtk.AttachOptions)(4));
			// Container child table2.Gtk.Table+TableChild
			this.convertWav = new global::Gtk.CheckButton();
			this.convertWav.CanFocus = true;
			this.convertWav.Name = "convertWav";
			this.convertWav.Label = global::Mono.Unix.Catalog.GetString("WAV (no metadata)");
			this.convertWav.DrawIndicator = true;
			this.convertWav.UseUnderline = true;
			this.table2.Add(this.convertWav);
			global::Gtk.Table.TableChild w34 = ((global::Gtk.Table.TableChild)(this.table2[this.convertWav]));
			w34.RightAttach = ((uint)(2));
			w34.YOptions = ((global::Gtk.AttachOptions)(4));
			w21.Add(this.table2);
			this.scrolledwindow2.Add(w21);
			this.notebook1.Add(this.scrolledwindow2);
			global::Gtk.Notebook.NotebookChild w37 = ((global::Gtk.Notebook.NotebookChild)(this.notebook1[this.scrolledwindow2]));
			w37.Position = 1;
			// Notebook tab
			this.label8 = new global::Gtk.Label();
			this.label8.Name = "label8";
			this.label8.LabelProp = global::Mono.Unix.Catalog.GetString("Convert");
			this.notebook1.SetTabLabel(this.scrolledwindow2, this.label8);
			this.label8.ShowAll();
			// Container child notebook1.Gtk.Notebook+NotebookChild
			this.scrolledwindow3 = new global::Gtk.ScrolledWindow();
			this.scrolledwindow3.CanFocus = true;
			this.scrolledwindow3.Name = "scrolledwindow3";
			this.scrolledwindow3.HscrollbarPolicy = ((global::Gtk.PolicyType)(2));
			// Container child scrolledwindow3.Gtk.Container+ContainerChild
			global::Gtk.Viewport w38 = new global::Gtk.Viewport();
			w38.ShadowType = ((global::Gtk.ShadowType)(0));
			// Container child GtkViewport2.Gtk.Container+ContainerChild
			this.table3 = new global::Gtk.Table(((uint)(6)), ((uint)(3)), false);
			this.table3.Name = "table3";
			this.table3.RowSpacing = ((uint)(2));
			this.table3.ColumnSpacing = ((uint)(2));
			// Container child table3.Gtk.Table+TableChild
			this.hseparator1 = new global::Gtk.HSeparator();
			this.hseparator1.Name = "hseparator1";
			this.table3.Add(this.hseparator1);
			global::Gtk.Table.TableChild w39 = ((global::Gtk.Table.TableChild)(this.table3[this.hseparator1]));
			w39.TopAttach = ((uint)(1));
			w39.BottomAttach = ((uint)(2));
			w39.RightAttach = ((uint)(3));
			w39.YOptions = ((global::Gtk.AttachOptions)(4));
			// Container child table3.Gtk.Table+TableChild
			this.label10 = new global::Gtk.Label();
			this.label10.Name = "label10";
			this.label10.Xalign = 0F;
			this.label10.LabelProp = global::Mono.Unix.Catalog.GetString("Playlist directory");
			this.table3.Add(this.label10);
			global::Gtk.Table.TableChild w40 = ((global::Gtk.Table.TableChild)(this.table3[this.label10]));
			w40.TopAttach = ((uint)(3));
			w40.BottomAttach = ((uint)(4));
			w40.XPadding = ((uint)(3));
			w40.XOptions = ((global::Gtk.AttachOptions)(4));
			w40.YOptions = ((global::Gtk.AttachOptions)(4));
			// Container child table3.Gtk.Table+TableChild
			this.label11 = new global::Gtk.Label();
			this.label11.Name = "label11";
			this.label11.Xalign = 0F;
			this.label11.LabelProp = global::Mono.Unix.Catalog.GetString("Playlist filename");
			this.table3.Add(this.label11);
			global::Gtk.Table.TableChild w41 = ((global::Gtk.Table.TableChild)(this.table3[this.label11]));
			w41.TopAttach = ((uint)(4));
			w41.BottomAttach = ((uint)(5));
			w41.XPadding = ((uint)(3));
			w41.XOptions = ((global::Gtk.AttachOptions)(4));
			w41.YOptions = ((global::Gtk.AttachOptions)(4));
			// Container child table3.Gtk.Table+TableChild
			this.otherMusicBrainz = new global::Gtk.CheckButton();
			this.otherMusicBrainz.CanFocus = true;
			this.otherMusicBrainz.Name = "otherMusicBrainz";
			this.otherMusicBrainz.Label = global::Mono.Unix.Catalog.GetString("MusicBrainz lookup");
			this.otherMusicBrainz.DrawIndicator = true;
			this.otherMusicBrainz.UseUnderline = true;
			this.table3.Add(this.otherMusicBrainz);
			global::Gtk.Table.TableChild w42 = ((global::Gtk.Table.TableChild)(this.table3[this.otherMusicBrainz]));
			w42.RightAttach = ((uint)(3));
			w42.YOptions = ((global::Gtk.AttachOptions)(4));
			// Container child table3.Gtk.Table+TableChild
			this.otherPlaylist = new global::Gtk.CheckButton();
			this.otherPlaylist.CanFocus = true;
			this.otherPlaylist.Name = "otherPlaylist";
			this.otherPlaylist.Label = global::Mono.Unix.Catalog.GetString("Create playlist");
			this.otherPlaylist.DrawIndicator = true;
			this.otherPlaylist.UseUnderline = true;
			this.table3.Add(this.otherPlaylist);
			global::Gtk.Table.TableChild w43 = ((global::Gtk.Table.TableChild)(this.table3[this.otherPlaylist]));
			w43.TopAttach = ((uint)(2));
			w43.BottomAttach = ((uint)(3));
			w43.RightAttach = ((uint)(3));
			w43.YOptions = ((global::Gtk.AttachOptions)(4));
			// Container child table3.Gtk.Table+TableChild
			this.otherPlaylistDirectory = new global::Gtk.Entry();
			this.otherPlaylistDirectory.CanFocus = true;
			this.otherPlaylistDirectory.Name = "otherPlaylistDirectory";
			this.otherPlaylistDirectory.IsEditable = true;
			this.otherPlaylistDirectory.InvisibleChar = '•';
			this.table3.Add(this.otherPlaylistDirectory);
			global::Gtk.Table.TableChild w44 = ((global::Gtk.Table.TableChild)(this.table3[this.otherPlaylistDirectory]));
			w44.TopAttach = ((uint)(3));
			w44.BottomAttach = ((uint)(4));
			w44.LeftAttach = ((uint)(1));
			w44.RightAttach = ((uint)(2));
			w44.YOptions = ((global::Gtk.AttachOptions)(4));
			// Container child table3.Gtk.Table+TableChild
			this.otherPlaylistDirectoryOpen = new global::Gtk.Button();
			this.otherPlaylistDirectoryOpen.CanFocus = true;
			this.otherPlaylistDirectoryOpen.Name = "otherPlaylistDirectoryOpen";
			this.table3.Add(this.otherPlaylistDirectoryOpen);
			global::Gtk.Table.TableChild w45 = ((global::Gtk.Table.TableChild)(this.table3[this.otherPlaylistDirectoryOpen]));
			w45.TopAttach = ((uint)(3));
			w45.BottomAttach = ((uint)(4));
			w45.LeftAttach = ((uint)(2));
			w45.RightAttach = ((uint)(3));
			w45.XOptions = ((global::Gtk.AttachOptions)(0));
			w45.YOptions = ((global::Gtk.AttachOptions)(0));
			// Container child table3.Gtk.Table+TableChild
			this.otherPlaylistFilename = new global::Gtk.Entry();
			this.otherPlaylistFilename.CanFocus = true;
			this.otherPlaylistFilename.Name = "otherPlaylistFilename";
			this.otherPlaylistFilename.IsEditable = true;
			this.otherPlaylistFilename.InvisibleChar = '•';
			this.table3.Add(this.otherPlaylistFilename);
			global::Gtk.Table.TableChild w46 = ((global::Gtk.Table.TableChild)(this.table3[this.otherPlaylistFilename]));
			w46.TopAttach = ((uint)(4));
			w46.BottomAttach = ((uint)(5));
			w46.LeftAttach = ((uint)(1));
			w46.RightAttach = ((uint)(3));
			w46.YOptions = ((global::Gtk.AttachOptions)(4));
			// Container child table3.Gtk.Table+TableChild
			this.otherPlaylistRelative = new global::Gtk.CheckButton();
			this.otherPlaylistRelative.CanFocus = true;
			this.otherPlaylistRelative.Name = "otherPlaylistRelative";
			this.otherPlaylistRelative.Label = global::Mono.Unix.Catalog.GetString("Relative paths in playlist");
			this.otherPlaylistRelative.DrawIndicator = true;
			this.otherPlaylistRelative.UseUnderline = true;
			this.table3.Add(this.otherPlaylistRelative);
			global::Gtk.Table.TableChild w47 = ((global::Gtk.Table.TableChild)(this.table3[this.otherPlaylistRelative]));
			w47.TopAttach = ((uint)(5));
			w47.BottomAttach = ((uint)(6));
			w47.RightAttach = ((uint)(3));
			w47.XPadding = ((uint)(10));
			w47.YOptions = ((global::Gtk.AttachOptions)(4));
			w38.Add(this.table3);
			this.scrolledwindow3.Add(w38);
			this.notebook1.Add(this.scrolledwindow3);
			global::Gtk.Notebook.NotebookChild w50 = ((global::Gtk.Notebook.NotebookChild)(this.notebook1[this.scrolledwindow3]));
			w50.Position = 2;
			// Notebook tab
			this.label9 = new global::Gtk.Label();
			this.label9.Name = "label9";
			this.label9.LabelProp = global::Mono.Unix.Catalog.GetString("Other");
			this.notebook1.SetTabLabel(this.scrolledwindow3, this.label9);
			this.label9.ShowAll();
			this.vbox1.Add(this.notebook1);
			global::Gtk.Box.BoxChild w51 = ((global::Gtk.Box.BoxChild)(this.vbox1[this.notebook1]));
			w51.Position = 0;
			// Container child vbox1.Gtk.Box+BoxChild
			this.hbuttonbox1 = new global::Gtk.HButtonBox();
			this.hbuttonbox1.Name = "hbuttonbox1";
			this.hbuttonbox1.Spacing = 5;
			this.hbuttonbox1.BorderWidth = ((uint)(5));
			this.hbuttonbox1.LayoutStyle = ((global::Gtk.ButtonBoxStyle)(4));
			// Container child hbuttonbox1.Gtk.ButtonBox+ButtonBoxChild
			this.button1 = new global::Gtk.Button();
			this.button1.CanFocus = true;
			this.button1.Name = "button1";
			this.button1.UseStock = true;
			this.button1.UseUnderline = true;
			this.button1.Label = "gtk-cancel";
			this.hbuttonbox1.Add(this.button1);
			global::Gtk.ButtonBox.ButtonBoxChild w52 = ((global::Gtk.ButtonBox.ButtonBoxChild)(this.hbuttonbox1[this.button1]));
			w52.Expand = false;
			w52.Fill = false;
			// Container child hbuttonbox1.Gtk.ButtonBox+ButtonBoxChild
			this.button2 = new global::Gtk.Button();
			this.button2.CanFocus = true;
			this.button2.Name = "button2";
			this.button2.UseStock = true;
			this.button2.UseUnderline = true;
			this.button2.Label = "gtk-apply";
			this.hbuttonbox1.Add(this.button2);
			global::Gtk.ButtonBox.ButtonBoxChild w53 = ((global::Gtk.ButtonBox.ButtonBoxChild)(this.hbuttonbox1[this.button2]));
			w53.Position = 1;
			w53.Expand = false;
			w53.Fill = false;
			// Container child hbuttonbox1.Gtk.ButtonBox+ButtonBoxChild
			this.button3 = new global::Gtk.Button();
			this.button3.CanFocus = true;
			this.button3.Name = "button3";
			this.button3.UseStock = true;
			this.button3.UseUnderline = true;
			this.button3.Label = "gtk-save";
			this.hbuttonbox1.Add(this.button3);
			global::Gtk.ButtonBox.ButtonBoxChild w54 = ((global::Gtk.ButtonBox.ButtonBoxChild)(this.hbuttonbox1[this.button3]));
			w54.Position = 2;
			w54.Expand = false;
			w54.Fill = false;
			this.vbox1.Add(this.hbuttonbox1);
			global::Gtk.Box.BoxChild w55 = ((global::Gtk.Box.BoxChild)(this.vbox1[this.hbuttonbox1]));
			w55.Position = 1;
			w55.Expand = false;
			w55.Fill = false;
			this.Add(this.vbox1);
			if ((this.Child != null))
			{
				this.Child.ShowAll();
			}
			this.Show();
			this.notebook1.SwitchPage += new global::Gtk.SwitchPageHandler(this.SwitchPage);
			this.ripSingleDirectoryOpen.Clicked += new global::System.EventHandler(this.RipDirectorySelect);
			this.ripMultiDirectoryOpen.Clicked += new global::System.EventHandler(this.RipDirectorySelect);
			this.otherPlaylistDirectoryOpen.Clicked += new global::System.EventHandler(this.PlaylistDirectorySelect);
			this.button1.Clicked += new global::System.EventHandler(this.Close);
			this.button2.Clicked += new global::System.EventHandler(this.Apply);
			this.button3.Clicked += new global::System.EventHandler(this.Save);
		}
	}
}
