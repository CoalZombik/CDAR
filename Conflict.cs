﻿using System;
using Gtk;
using Mono.Unix;
using MetaBrainz.MusicBrainz;
using MetaBrainz.MusicBrainz.DiscId;

namespace CDAR
{
	public partial class Conflict : Dialog
	{
		public Album SelectedAlbum { get { return albums[selectRelease.Active]; } }
		public string[] SelectedArtists { get { return artists[selectRelease.Active]; } }
		public string[] SelectedGenres { get { return genres[selectRelease.Active]; } }

		Query query;
		TableOfContents data;
		int length;
		Guid[] ids;

		Album[] albums;
		string[][] artists;
		string[][] genres;

		NodeStore dataStore;

		public Conflict (Query query, TableOfContents data, Guid[] ids, string[] info)
		{
			Build();
			Icon = Gdk.Pixbuf.LoadFromResource("cdar_icon");

			length = ids.Length;
			this.query = query;
			this.data = data;
			this.ids = ids;
			albums = new Album[length];
			artists = new string[length][];
			genres = new string[length][];

			dataStore = new NodeStore(typeof(KeyValueNode));
			dataRelease.NodeStore = dataStore;

			dataRelease.AppendColumn(Catalog.GetString("Attribute"), new CellRendererText(), "text", 0);
			dataRelease.AppendColumn(Catalog.GetString("Value"), new CellRendererText(), "text", 1);

			for (int i = 0; i < length; i++)
				selectRelease.AppendText(info[i]);

			selectRelease.Active = 0;
		}

		protected void Select_Changed (object sender, EventArgs e)
		{
			UpdateData(selectRelease.Active);
		}

		void UpdateData (int id)
		{
			dataStore.Clear();
			Album album;
			if (albums[id] == null)
			{
				album = MainClass.GetRelease(query, data, ids[id], out artists[id], out genres[id]);
				if (album != null)
					albums[id] = album;
				else
				{
					buttonOk.Sensitive = false;
					return;
				}
			}
			else
				album = albums[id];

			buttonOk.Sensitive = true;

			dataStore.AddNode(new KeyValueNode(Catalog.GetString("Album title"), album.Name));
			if (artists[id] != null && artists[id].Length > 0)
			{
				int artistsLenght = artists[id].Length;
				dataStore.AddNode(new KeyValueNode(Catalog.GetString("Artist"), artists[id][0]));
				for (int i = 1; i < artistsLenght; i++)
					dataStore.AddNode(new KeyValueNode("", artists[id][i]));
			}

			dataStore.AddNode(new KeyValueNode(Catalog.GetString("Year"), album.Year));

			if (genres[id] != null && genres[id].Length > 0)
			{
				int genresLenght = genres[id].Length;
				dataStore.AddNode(new KeyValueNode(Catalog.GetString("Genre"), genres[id][0]));
				for (int i = 1; i < genresLenght; i++)
					dataStore.AddNode(new KeyValueNode("", genres[id][i]));
			}

			for (int i = 0; i < album.Tracks.Length; i++)
			{
				Track track = album.Tracks[i];
				dataStore.AddNode(new KeyValueNode(string.Format(Catalog.GetString("Track {0}"), track.Number.ToString("D2")), string.Format("{0} ({1})", track.Name, track.LengthText)));
			}
		}
	}
}