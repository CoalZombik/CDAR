﻿using System;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using Gtk;
using Mono.Unix;

namespace CDAR
{
	public static class Rip
	{
		//https://en.wikipedia.org/wiki/Track_(optical_disc)#Audio_tracks
		public const int SAMPLES_IN_SECTOR = 1176;
		public const int SECTORS_IN_SECOND = 75;
		public const float PROGRESS_CHANGE = 0.005f;

		const string TEMP_DIR = "/tmp/cdar/";

		public static bool Working { get; private set; }

		static Thread ripThread, convertThread;
		static Process ripProcess, convertProcess;
		static Album ripAlbum;
		static int nextTrack = 1, convertTrack, trackCount;
		static string directory, ripParameters;

		public static void RipAlbum (Album album)
		{
			if (!MainClass.CheckCommand("cdparanoia"))
			{
				NoParanoiaDialog();
				return;
			}

			Working = true;

			trackCount = album.Tracks.Length;
			ripAlbum = album;

			ripParameters = "-ed " + Config.Data.Device;
			if (Config.Data.Speed > 0)
				ripParameters += " -S " + Config.Data.Speed;

			switch (Config.Data.Checking)
			{
				case RipChecking.Limited:
					ripParameters += " -Y";
					break;
				case RipChecking.None:
					ripParameters += " -Z";
					break;
			}

			if (Config.Data.FlacEnable && !MainClass.CheckCommand("flac"))
			{
				Config.Data.FlacEnable = false;
				ConvertWarningDialog("FLAC");
			}

			if (Config.Data.VorbisEnable && !MainClass.CheckCommand("oggenc"))
			{
				Config.Data.VorbisEnable = false;
				ConvertWarningDialog("OGG (Vorbis)");
			}

			if (Config.Data.Mp3Enable && !MainClass.CheckCommand("lame"))
			{
				Config.Data.Mp3Enable = false;
				ConvertWarningDialog("MP3");
			}

			if (Config.Data.AacEnable && !MainClass.CheckCommand("faac"))
			{
				Config.Data.AacEnable = false;
				ConvertWarningDialog("AAC");
			}

			if (album.Disc < 1)
				directory = album.InsertVariables(Config.Data.SingleDirectory);
			else
				directory = album.InsertVariables(Config.Data.MultiDirectory);
			if (!Directory.Exists(directory))
				Directory.CreateDirectory(directory);

			nextTrack = 1;
			string waittext = Catalog.GetString("Waiting");
			string skippedtext = Catalog.GetString("Skipped");
			for (int i = 1; i <= album.Tracks.Length; i++)
			{
				if (album.Tracks[i - 1].Active)
					MainClass.Win.UpdateProgress(i, 0, waittext);
				else
					MainClass.Win.CompleteProgress(i, skippedtext);
			}

			if (!Directory.Exists(TEMP_DIR))
				Directory.CreateDirectory(TEMP_DIR);
			DeleteAllTemp();

			RipNext();
		}

		static void NoParanoiaDialog ()
		{
			string dialogtext = Catalog.GetString("Command cdparanoia couldn't be found!\n" +
				"CD cannot be ripped. Please install cdparanoia package.");
			MessageDialog dialog = new MessageDialog(MainClass.Win, DialogFlags.Modal, MessageType.Error, ButtonsType.Ok, dialogtext);
			dialog.Run();
			dialog.Destroy();
		}

		static void ConvertWarningDialog (string type)
		{
			string dialogtext = string.Format(Catalog.GetString("Required command to converting audio to {0} couldn't be found.\n" +
																"Converting to this format will be skipped."), type);
			MessageDialog dialog = new MessageDialog(MainClass.Win, DialogFlags.Modal, MessageType.Warning, ButtonsType.Ok, dialogtext);
			dialog.Run();
			dialog.Destroy();
		}

		public static void Stop ()
		{
			if (!Working)
				return;

			if (ripThread != null && ripThread.IsAlive)
			{
				ripThread.Abort();

				if (!ripProcess.HasExited)
					ripProcess.Kill();

				MainClass.Win.UpdateProgress(nextTrack - 1, 0, "");
			}

			if (convertThread != null && convertThread.IsAlive)
			{
				convertThread.Abort();

				if (!convertProcess.HasExited)
					convertProcess.Kill();

				MainClass.Win.UpdateProgress(convertTrack, 0, "");
			}

			for (int i = nextTrack; i <= trackCount; i++)
			{
				if (ripAlbum.Tracks[i - 1].Active)
					MainClass.Win.UpdateProgress(i, 0, "");
			}

			DeleteAllTemp();
			End();
		}

		static void DeleteAllTemp ()
		{
			string[] files = Directory.GetFiles(TEMP_DIR);
			foreach (string file in files)
				File.Delete(file);
		}

		static void RipNext ()
		{
			while (ripThread != null && ripThread.IsAlive)
				Thread.Sleep(200);

			if (!Working)
				return;

			int track = nextTrack;
			nextTrack++;

			if (track > trackCount)
			{
				if (convertThread == null || !convertThread.IsAlive)
					Complete();
				return;
			}

			if (!ripAlbum.Tracks[track - 1].Active)
			{
				RipNext();
				return;
			}

			string basename = Path.Combine(directory, ripAlbum.InsertVariables(Config.Data.Filename, track));
			bool exist = false;

			if (Config.Data.WavEnable && File.Exists(basename + ".wav"))
				exist = true;
			if (Config.Data.FlacEnable && File.Exists(basename + ".flac"))
				exist = true;
			if (Config.Data.VorbisEnable && File.Exists(basename + ".ogg"))
				exist = true;
			if (Config.Data.Mp3Enable && File.Exists(basename + ".mp3"))
				exist = true;
			if (Config.Data.AacEnable && File.Exists(basename + ".m4a"))
				exist = true;

			if (exist)
			{
				string dialogtext = string.Format(Catalog.GetString("One or more of output files from track {0} exist.\n" +
																	"Would you like to replace these files?"), track.ToString("D2"));
				MessageDialog existsDialog = new MessageDialog(MainClass.Win, DialogFlags.Modal, MessageType.Question, ButtonsType.YesNo, dialogtext);
				ResponseType dialogOutput = (ResponseType)existsDialog.Run();
				existsDialog.Destroy();
				if (dialogOutput != ResponseType.Yes)
				{
					MainClass.Win.CompleteProgress(track, Catalog.GetString("Skipped"));
					RipNext();
					return;
				}
			}

			MainClass.Win.UpdateProgress(track, 0, Catalog.GetString("Ripping"));
			ripThread = new Thread(() => RipOne(track));
			ripThread.Start();
		}

		static void RipOne (int track)
		{
			string filename = Path.Combine(TEMP_DIR, ripAlbum.InsertVariables(Config.Data.Filename, track) + ".wav");
			ProcessStartInfo info = new ProcessStartInfo("cdparanoia", ripParameters + " " + track + " \"" + filename + "\"");
			info.CreateNoWindow = true;
			info.UseShellExecute = false;
			info.RedirectStandardError = true;
			ripProcess = Process.Start(info);
			StreamReader output = ripProcess.StandardError;
			int startSample = 0, endSample = 0;
			float lastProgress = 0;

			while (endSample == 0)
			{
				string line = output.ReadLine();
				if (!line.StartsWith("Ripping from sector"))
					continue;

				Match match = Regex.Match(line, @"\d+\s\(");
				string start = match.Value;
				start = start.Remove(start.Length - 2);
				startSample = int.Parse(start) * SAMPLES_IN_SECTOR;

				line = output.ReadLine();
				match = Regex.Match(line, @"\d+\s\(");
				string end = match.Value;
				end = end.Remove(end.Length - 2);
				endSample = (int.Parse(end) + 1) * SAMPLES_IN_SECTOR;
			}

			bool Valid = false;

			while (true)
			{
				if (output.EndOfStream)
				{
					if (ripProcess.HasExited)
						break;
					Thread.Sleep(25);
					continue;
				}

				string line = output.ReadLine();

				//TODO: Process other informations
				if (line.Begins("##: 0")) //read
				{
					int sample = int.Parse(line.Substring(15)); //##: 0 [read] @ 
					float progress = (sample - startSample) / (float)(endSample - startSample);
					if (progress - lastProgress > PROGRESS_CHANGE)
					{
						lastProgress = progress;
						if (progress >= 1) //don't show Complete
							progress = 0.999f;
						Application.Invoke(delegate { MainClass.Win.UpdateProgress(track, progress * 100); });
					}
				}
				else if (line.Begins("##: -1")) //finished
				{
					Valid = true;
					break;
				}
			}

			ripProcess.WaitForExit();
			if (ripProcess.ExitCode != 0) //for certainty
				Valid = false;
			ripProcess.Close();

			if (Valid)
			{
				if (convertThread != null && convertThread.IsAlive)
				{
					Application.Invoke(delegate { MainClass.Win.UpdateProgress(track, 0, Catalog.GetString("Waiting to convert")); });
					while (convertThread != null && convertThread.IsAlive)
						Thread.Sleep(200);
				}

				Application.Invoke(delegate { RipNext(); });
				convertThread = new Thread(() => ConvertOne(track));
				convertThread.Start();
			}
			else
				Application.Invoke(delegate { ErrorRip(track); });
		}

		static void ConvertOne (int track)
		{
			if (!Working)
				return;

			convertTrack = track;
			string basename = ripAlbum.InsertVariables(Config.Data.Filename, track);
			string outputname = Path.Combine(directory, basename);
			string tempname = Path.Combine(TEMP_DIR, basename);
			string filename = tempname + ".wav";
			Track data = ripAlbum.Tracks[track - 1];

			Application.Invoke(delegate { MainClass.Win.UpdateProgress(track, 0, Catalog.GetString("Converting")); });

			float progress = 0;
			float progressStep = 100f / ((Config.Data.FlacEnable ? 1 : 0) + (Config.Data.VorbisEnable ? 1 : 0) + (Config.Data.Mp3Enable ? 1 : 0) + (Config.Data.AacEnable ? 1 : 0));

			if (Config.Data.FlacEnable)
			{
				string flacfile = outputname + ".flac";
				string flactemp = tempname + ".flac";
				string parameters = string.Format("--totally-silent \"{0}\" -{1} -T TRACKNUMBER={2} -T TITLE=\"{3}\" -T ALBUM=\"{4}\"", filename, Config.Data.FlacCompress, data.Number.ToString("D2"), data.Name, ripAlbum.Name);
				if (!string.IsNullOrWhiteSpace(ripAlbum.Artist))
					parameters += " -T ARTIST=\"" + ripAlbum.Artist + "\"";
				if (!string.IsNullOrWhiteSpace(ripAlbum.Year))
					parameters += " -T DATE=\"" + ripAlbum.Year + "\"";
				if (!string.IsNullOrWhiteSpace(ripAlbum.Genre))
					parameters += " -T GENRE=\"" + ripAlbum.Genre + "\"";
				parameters += " -o \"" + flactemp + "\"";
				ProcessStartInfo info = new ProcessStartInfo("flac", parameters);
				info.UseShellExecute = false;
				convertProcess = Process.Start(info);
				convertProcess.WaitForExit();
				if (convertProcess.ExitCode == 0)
				{
					if (File.Exists(flacfile))
						File.Delete(flacfile);
					File.Move(flactemp, flacfile);
					progress += progressStep;
					Application.Invoke(delegate { MainClass.Win.UpdateProgress(track, progress, null); });
				}
				else
				{
					Application.Invoke(delegate { ErrorConvert(track, flactemp); });
					return;
				}
			}

			if (Config.Data.VorbisEnable)
			{
				string vorbisfile = outputname + ".ogg";
				string vorbistemp = tempname + ".ogg";
				string parameters = string.Format("--quiet -q {0} -N {1} -t \"{2}\" -l \"{3}\"", Config.Data.VorbisQuality, data.Number.ToString("D2"), data.Name, ripAlbum.Name);
				if (!string.IsNullOrWhiteSpace(ripAlbum.Artist))
					parameters += " -a \"" + ripAlbum.Artist + "\"";
				if (!string.IsNullOrWhiteSpace(ripAlbum.Year))
					parameters += " -d \"" + ripAlbum.Year + "\"";
				if (!string.IsNullOrWhiteSpace(ripAlbum.Genre))
					parameters += " -G \"" + ripAlbum.Genre + "\"";
				parameters += string.Format(" \"{0}\" -o \"{1}\"", filename, vorbistemp);
				ProcessStartInfo info = new ProcessStartInfo("oggenc", parameters);
				info.UseShellExecute = false;
				convertProcess = Process.Start(info);
				convertProcess.WaitForExit();
				if (convertProcess.ExitCode == 0)
				{
					if (File.Exists(vorbisfile))
						File.Delete(vorbisfile);
					File.Move(vorbistemp, vorbisfile);
					progress += progressStep;
					Application.Invoke(delegate { MainClass.Win.UpdateProgress(track, progress, null); });
				}
				else
				{
					Application.Invoke(delegate { ErrorConvert(track, vorbistemp); });
					return;
				}
			}

			if (Config.Data.Mp3Enable)
			{
				string mp3file = outputname + ".mp3";
				string mp3temp = tempname + ".mp3";
				string parameters = string.Format("--quiet -V {0} --tn {1} --tt \"{2}\" --tl \"{3}\"", Config.Data.Mp3Quality, data.Number.ToString("D2"), data.Name, ripAlbum.Name);
				if (!string.IsNullOrWhiteSpace(ripAlbum.Artist))
					parameters += " --ta \"" + ripAlbum.Artist + "\"";
				if (!string.IsNullOrWhiteSpace(ripAlbum.Year))
					parameters += " --ty \"" + ripAlbum.Year + "\"";
				if (!string.IsNullOrWhiteSpace(ripAlbum.Genre))
					parameters += " --tg \"" + ripAlbum.Genre + "\"";
				parameters += string.Format(" \"{0}\" \"{1}\"", filename, mp3temp);
				ProcessStartInfo info = new ProcessStartInfo("lame", parameters);
				info.UseShellExecute = false;
				convertProcess = Process.Start(info);
				convertProcess.WaitForExit();
				if (convertProcess.ExitCode == 0)
				{
					if (File.Exists(mp3file))
						File.Delete(mp3file);
					File.Move(mp3temp, mp3file);
					progress += progressStep;
					Application.Invoke(delegate { MainClass.Win.UpdateProgress(track, progress, null); });
				}
				else
				{
					Application.Invoke(delegate { ErrorConvert(track, mp3temp); });
					return;
				}
			}

			if (Config.Data.AacEnable)
			{
				string aacfile = outputname + ".m4a";
				string aactemp = tempname + ".m4a";
				string parameters = string.Format("-v 0 -w -q {0} --track {1} --title \"{2}\" --album \"{3}\"", Config.Data.AacQuality * 100, data.Number.ToString("D2"), data.Name, ripAlbum.Name);
				if (!string.IsNullOrWhiteSpace(ripAlbum.Artist))
					parameters += " --artist \"" + ripAlbum.Artist + "\"";
				if (!string.IsNullOrWhiteSpace(ripAlbum.Year))
					parameters += " --year \"" + ripAlbum.Year + "\"";
				if (!string.IsNullOrWhiteSpace(ripAlbum.Genre))
					parameters += " --tag genre,\"" + ripAlbum.Genre + "\"";
				parameters += string.Format(" \"{0}\" -o \"{1}\"", filename, aactemp);
				ProcessStartInfo info = new ProcessStartInfo("faac", parameters);
				info.UseShellExecute = false;
				info.RedirectStandardError = true;
				convertProcess = Process.Start(info);
				convertProcess.WaitForExit();
				if (convertProcess.ExitCode == 0)
				{
					if (File.Exists(aacfile))
						File.Delete(aacfile);
					File.Move(aactemp, aacfile);
					progress += progressStep;
					Application.Invoke(delegate { MainClass.Win.UpdateProgress(track, progress, null); });
				}
				else
				{
					Application.Invoke(delegate { ErrorConvert(track, aactemp); });
					return;
				}
			}

			if (Config.Data.WavEnable)
			{
				string wavfile = outputname + ".wav";
				if (File.Exists(wavfile))
					File.Delete(wavfile);
				File.Move(filename, wavfile);
			}
			else
				File.Delete(filename);

			Application.Invoke(delegate { MainClass.Win.CompleteProgress(track); });

			if (nextTrack - 1 > trackCount)
				Application.Invoke(delegate { Complete(); });
		}

		static void Playlist ()
		{
			string outputdir = ripAlbum.InsertVariables(Config.Data.PlaylistDirectory);
			try
			{
				if (!Directory.Exists(outputdir))
					Directory.CreateDirectory(outputdir);
			}
			catch
			{
				string message = string.Format(Catalog.GetString("Playlist directory cannot be created: <i>{0}</i>.\n" +
																 "Check permissions to parent directory."), outputdir);
				MessageDialog dialog = new MessageDialog(MainClass.Win, DialogFlags.Modal, MessageType.Error, ButtonsType.Ok, message);
				dialog.Run();
				dialog.Destroy();
				return;
			}
			string basename = directory;
			if (Config.Data.PlaylistRelative)
				basename = MainClass.RelativePath(outputdir, basename);
			string filename = Path.Combine(outputdir, ripAlbum.InsertVariables(Config.Data.PlaylistFilename));
			if (Config.Data.WavEnable)
				PlaylistOne(basename, ".wav", filename + ".wav.m3u8");
			if (Config.Data.FlacEnable)
				PlaylistOne(basename, ".flac", filename + ".flac.m3u8");
			if (Config.Data.VorbisEnable)
				PlaylistOne(basename, ".ogg", filename + ".ogg.m3u8");
			if (Config.Data.Mp3Enable)
				PlaylistOne(basename, ".mp3", filename + ".mp3.m3u8");
			if (Config.Data.AacEnable)
				PlaylistOne(basename, ".m4a", filename + ".m4a.m3u8");
		}

		static void PlaylistOne (string basename, string extension, string filename)
		{
			StringBuilder playlist = new StringBuilder("#EXTM3U\n");
			for (int i = 0; i < ripAlbum.Tracks.Length; i++)
			{
				if (!ripAlbum.Tracks[i].Active)
					continue;
				string trackname = ripAlbum.InsertVariables(Config.Data.Filename, i + 1);
				playlist.AppendFormat("#EXTINF:{0},{1}\n", Math.Round(ripAlbum.Tracks[i].Seconds), trackname);
				playlist.Append(Path.Combine(basename, trackname + extension) + "\n");
			}

			try
			{
				if (File.Exists(filename))
				{
					string message = string.Format(Catalog.GetString("Playlist file exists: <i>{0}</i>.\n" +
																	 "Would you like to replace this file?"), filename);
					MessageDialog dialog = new MessageDialog(MainClass.Win, DialogFlags.Modal, MessageType.Question, ButtonsType.YesNo, message);
					ResponseType response = (ResponseType)dialog.Run();
					dialog.Destroy();
					if (response != ResponseType.Yes)
						return;
					File.Delete(filename);
				}

				File.WriteAllText(filename, playlist.ToString(), Encoding.UTF8);
			}
			catch
			{
				string message = string.Format(Catalog.GetString("Playlist cannot be saved to: <i>{0}</i>.\n" +
																 "Check permissions to this directory."), filename);
				MessageDialog dialog = new MessageDialog(MainClass.Win, DialogFlags.Modal, MessageType.Error, ButtonsType.Ok, message);
				dialog.Run();
				dialog.Destroy();
			}
		}

		static void ErrorRip (int track)
		{
			string filename = Path.Combine(TEMP_DIR, ripAlbum.InsertVariables(Config.Data.Filename, track) + ".wav");
			if (File.Exists(filename))
				File.Delete(filename);

			MainClass.Win.UpdateProgress(track, 0, Catalog.GetString("Rip error"));
			string dialogtext = string.Format(Catalog.GetString("Rip error in track {0}.\n" +
																"Check if the CD is not damaged and inserted correctly into the drive."), track.ToString("D2"));
			MessageDialog dialog = new MessageDialog(MainClass.Win, DialogFlags.Modal, MessageType.Error, ButtonsType.Ok, dialogtext);
			dialog.Run();
			dialog.Destroy();

			End();
		}

		static void ErrorConvert (int track, string filename)
		{
			if (File.Exists(filename))
				File.Delete(filename);

			MainClass.Win.UpdateProgress(track, 0, Catalog.GetString("Convert error"));
			string dialogtext = string.Format(Catalog.GetString("Convert error in file <i>{0}</i>."), filename);
			MessageDialog dialog = new MessageDialog(MainClass.Win, DialogFlags.Modal, MessageType.Error, ButtonsType.Ok, dialogtext);
			dialog.Run();
			dialog.Destroy();
		}

		static void Complete ()
		{
			if (Config.Data.PlaylistCreate)
				Playlist();

			if (Config.Data.Eject)
				Process.Start("eject", Config.Data.Device);

			MessageDialog dialog = new MessageDialog(MainClass.Win, DialogFlags.Modal, MessageType.Info, ButtonsType.Ok, Catalog.GetString("Rip completed!"));
			dialog.Run();
			dialog.Destroy();

			End();
		}

		static void End ()
		{
			Working = false;
			MainClass.Win.Rip_Ended();
		}
	}
}
