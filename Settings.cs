﻿using System;
using System.IO;
using Gtk;
using Mono.Unix;

namespace CDAR
{
	public partial class Settings : Window
	{
		public Settings () : base(WindowType.Toplevel)
		{
			Build();
			Icon = Gdk.Pixbuf.LoadFromResource("cdar_icon");

			ripSingleDirectoryOpen.Image = new Image("gtk-open", IconSize.Button);
			ripMultiDirectoryOpen.Image = new Image("gtk-open", IconSize.Button);
			ripSpeed.TooltipText = Catalog.GetString("0 = default");

			convertWav.TooltipText = Catalog.GetString("Uncompressed format");
			convertFlac.TooltipText = Catalog.GetString("Free lossless compressed format");
			convertVorbis.TooltipText = Catalog.GetString("Free loss compressed format");
			convertMp3.TooltipText = Catalog.GetString("Obsolete loss compressed format");
			convertAac.TooltipText = Catalog.GetString("Loss compressed format");

			Load();
		}

		void Load ()
		{
			ripDevice.Text = Config.Data.Device;
			ripSpeed.Value = Config.Data.Speed;
			ripChecking.Active = (int)Config.Data.Checking;
			ripEject.Active = Config.Data.Eject;
			ripSingleDirectory.Text = Config.Data.SingleDirectory;
			ripMultiDirectory.Text = Config.Data.MultiDirectory;
			ripFilename.Text = Config.Data.Filename;

			convertWav.Active = Config.Data.WavEnable;

			string installtext = "\n" + Catalog.GetString("Package <i>{0}</i> must be installed.");

			if (MainClass.CheckCommand("flac")) //flac
				convertFlac.Active = Config.Data.FlacEnable;
			else
			{
				convertFlac.Active = false;
				convertFlac.TooltipMarkup += "\n" + string.Format(installtext, "flac");
				convertFlac.Sensitive = false;
				convertFlacLabel.Sensitive = false;
				convertFlacCompress.Sensitive = false;
			}
			convertFlacCompress.Value = Config.Data.FlacCompress;

			if (MainClass.CheckCommand("oggenc")) //vorbis-tools
				convertVorbis.Active = Config.Data.VorbisEnable;
			else
			{
				convertVorbis.Active = false;
				convertVorbis.TooltipMarkup += string.Format(installtext, "vorbis-tools");
				convertVorbis.Sensitive = false;
				convertVorbisLabel.Sensitive = false;
				convertVorbisQuality.Sensitive = false;
			}
			convertVorbisQuality.Value = Config.Data.VorbisQuality;

			if (MainClass.CheckCommand("lame")) //lame
				convertMp3.Active = Config.Data.Mp3Enable;
			else
			{
				convertMp3.Active = false;
				convertMp3.TooltipMarkup += string.Format(installtext, "lame");
				convertMp3.Sensitive = false;
				convertMp3Label.Sensitive = false;
				convertMp3Quality.Sensitive = false;
			}
			convertMp3Quality.Value = Config.Data.Mp3Quality;

			if (MainClass.CheckCommand("faac")) //faac
				convertAac.Active = Config.Data.AacEnable;
			else
			{
				convertAac.Active = false;
				convertAac.TooltipMarkup += string.Format(installtext, "faac");
				convertAac.Sensitive = false;
				convertAacLabel.Sensitive = false;
				convertAacQuality.Sensitive = false;
			}
			convertAacQuality.Value = Config.Data.AacQuality;

			otherMusicBrainz.Active = Config.Data.MusicBrainzEnable;

			otherPlaylist.Active = Config.Data.PlaylistCreate;
			otherPlaylistDirectory.Text = Config.Data.PlaylistDirectory;
			otherPlaylistFilename.Text = Config.Data.PlaylistFilename;
			otherPlaylistRelative.Active = Config.Data.PlaylistRelative;
		}

		void Apply (object sender, EventArgs e)
		{
			Apply();
		}

		void Apply ()
		{
			Config.Data.Device = ripDevice.Text;
			Config.Data.Speed = ripSpeed.ValueAsInt;
			Config.Data.Checking = (RipChecking)ripChecking.Active;
			Config.Data.Eject = ripEject.Active;
			Config.Data.SingleDirectory = ripSingleDirectory.Text;
			Config.Data.MultiDirectory = ripMultiDirectory.Text;
			Config.Data.Filename = ripFilename.Text;

			Config.Data.WavEnable = convertWav.Active;
			Config.Data.FlacEnable = convertFlac.Active;
			Config.Data.FlacCompress = (int)convertFlacCompress.Value;
			Config.Data.VorbisEnable = convertVorbis.Active;
			Config.Data.VorbisQuality = (int)convertVorbisQuality.Value;
			Config.Data.Mp3Enable = convertMp3.Active;
			Config.Data.Mp3Quality = (int)convertVorbisQuality.Value;
			Config.Data.AacEnable = convertAac.Active;
			Config.Data.AacQuality = (float)convertAacQuality.Value;

			Config.Data.MusicBrainzEnable = otherMusicBrainz.Active;

			Config.Data.PlaylistCreate = otherPlaylist.Active;
			Config.Data.PlaylistDirectory = otherPlaylistDirectory.Text;
			Config.Data.PlaylistFilename = otherPlaylistFilename.Text;
			Config.Data.PlaylistRelative = otherPlaylistRelative.Active;

			Config.Save();
		}

		void Save (object sender, EventArgs e)
		{
			Apply();
			Destroy();
		}

		protected void Close (object sender, EventArgs e)
		{
			Destroy();
		}

		protected void RipDirectorySelect (object sender, EventArgs e)
		{
			FileChooserDialog dialog = new FileChooserDialog(Catalog.GetString("Select output directory"), this, FileChooserAction.SelectFolder, "gtk-cancel", ResponseType.No, "gtk-ok", ResponseType.Yes);
			if ((ResponseType)dialog.Run() == ResponseType.Yes && Directory.Exists(dialog.CurrentFolder))
			{
				if (sender == ripSingleDirectoryOpen)
					ripSingleDirectory.Text = dialog.CurrentFolder;
				else if (sender == ripMultiDirectoryOpen)
					ripMultiDirectory.Text = dialog.CurrentFolder;
			}
			dialog.Destroy();
		}

		protected void PlaylistDirectorySelect (object sender, EventArgs e)
		{
			FileChooserDialog dialog = new FileChooserDialog(Catalog.GetString("Select playlist directory"), this, FileChooserAction.SelectFolder, "gtk-cancel", ResponseType.No, "gtk-ok", ResponseType.Yes);
			if ((ResponseType)dialog.Run() == ResponseType.Yes && Directory.Exists(dialog.CurrentFolder))
				otherPlaylistDirectory.Text = dialog.CurrentFolder;
			dialog.Destroy();
		}

		//Enforce to show image on button
		protected void SwitchPage (object o, SwitchPageArgs args)
		{
			if (args.PageNum == 2)
				otherPlaylistDirectoryOpen.Image = new Image("gtk-open", IconSize.Button);
		}
	}
}
