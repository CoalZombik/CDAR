using System;
using Gtk;

namespace CDAR
{
	[TreeNode(ListOnly = true)]
	public class Track : TreeNode
	{
		[TreeNodeValue(Column = 0)]
		public bool Active = true;

		public int Number;

		[TreeNodeValue(Column = 1)]
		public string NumberText { get { return Number.ToString("D2"); } }

		[TreeNodeValue(Column = 2)]
		public string Name;

		public int Sectors;

		[TreeNodeValue(Column = 3)]
		public string LengthText
		{
			get
			{
				int seconds = Sectors / Rip.SECTORS_IN_SECOND;
				return (seconds / 60).ToString("D2") + ":" +
					(seconds % 60).ToString("D2") + "." +
					((Sectors % Rip.SECTORS_IN_SECOND) * 100 / Rip.SECTORS_IN_SECOND).ToString("D2");
			}
		}

		public float Seconds { get { return (Sectors / (float)Rip.SECTORS_IN_SECOND); } }

		[TreeNodeValue(Column = 4)]
		public float Progress = 0;

		public string ProgressPrefix = "";

		[TreeNodeValue(Column = 5)]
		public string ProgressText
		{
			get
			{
				if (Progress > 0 && Progress < 100)
				{
					string output = ProgressPrefix;
					if (!string.IsNullOrEmpty(output))
						output += ": ";
					output += (int)Progress + " %";
					return output;
				}
				else
					return ProgressPrefix;
			}
		}

		public Track (int number, string name, int sectors)
		{
			Number = number;
			Name = name;
			Sectors = sectors;
		}

		public Track (Track data)
		{
			Active = data.Active;
			Number = data.Number;
			Name = data.Name;
			Sectors = data.Sectors;
		}
	}

	public class Album
	{
		public string Name;
		public int Disc;
		public string Artist;
		public string Year;
		public string Genre;
		public Track[] Tracks;

		public Album (Track[] tracks)
		{
			Name = "";
			Disc = 0;
			Artist = "";
			Year = "";
			Genre = "";
			Tracks = tracks;
		}

		public Album (string name, int disc, string artist, string year, string genre, Track[] tracks)
		{
			Name = name;
			Disc = disc;
			Artist = artist;
			Year = year;
			Genre = genre;
			Tracks = tracks;
		}

		public string InsertVariables (string input)
		{
			return input.Replace("%album%", Name).Replace("%artist%", Artist).Replace("%year%", Year).Replace("%genre%", Genre).Replace("%disc%", Disc.ToString());
		}

		public string InsertVariables (string input, int track)
		{
			return InsertVariables(input).Replace("%track%", Tracks[track - 1].NumberText).Replace("%title%", Tracks[track - 1].Name);
		}
	}

	[TreeNode(ListOnly = true)]
	public class KeyValueNode : TreeNode
	{
		[TreeNodeValue(Column = 0)]
		public string Key;

		[TreeNodeValue(Column = 1)]
		public string Value;

		public KeyValueNode (string key, string value)
		{
			Key = key;
			Value = value;
		}
	}
}
