﻿using System;
using Gtk;
using Mono.Unix;
using CDAR;

public partial class MainWindow : Window
{
	NodeStore trackStore;
	int trackCount = 0;

	CellRendererToggle activeRenderer;
	CellRendererText nameRenderer;

	public MainWindow () : base(WindowType.Toplevel)
	{
		Build();
		Icon = Gdk.Pixbuf.LoadFromResource("cdar_icon");

		trackStore = new NodeStore(typeof(Track));
		trackList.NodeStore = trackStore;

		activeRenderer = new CellRendererToggle();
		activeRenderer.Activatable = true;
		activeRenderer.Toggled += TrackActive_Toogled;
		TreeViewColumn activeColumn = trackList.AppendColumn(Catalog.GetString("Rip"), activeRenderer, "active", 0);
		TreeViewColumn numberColumn = trackList.AppendColumn(Catalog.GetString("Number"), new CellRendererText(), "text", 1);
		numberColumn.Resizable = true;
		nameRenderer = new CellRendererText();
		nameRenderer.Editable = true;
		nameRenderer.Edited += TrackName_Edited;
		TreeViewColumn nameColumn = trackList.AppendColumn(Catalog.GetString("Name"), nameRenderer, "text", 2);
		nameColumn.Resizable = true;
		nameColumn.Expand = true;
		TreeViewColumn lengthColumn = trackList.AppendColumn(Catalog.GetString("Length"), new CellRendererText(), "text", 3);
		lengthColumn.Resizable = true;
		TreeViewColumn progressColumn = trackList.AppendColumn(Catalog.GetString("Progress"), new CellRendererProgress(), "value", 4, "text", 5);
		progressColumn.Resizable = true;
		progressColumn.MinWidth = 125;

		albumDiscNumber.TooltipText = Catalog.GetString("0 = single CD");
		albumGenre.WidthRequest = 150;

		refreshAction.Tooltip = Catalog.GetString("Refresh");
		cdromAction.Tooltip = Catalog.GetString("Rip");
		preferencesAction.Tooltip = Catalog.GetString("Settings");
	}

	void TrackName_Edited (object o, EditedArgs args)
	{
		Track node = trackStore.GetNode(new TreePath(args.Path)) as Track;
		node.Name = args.NewText;
	}

	void TrackActive_Toogled (object o, ToggledArgs args)
	{
		Track node = trackStore.GetNode(new TreePath(args.Path)) as Track;
		node.Active = !node.Active;
	}

	public void ImportAlbum (Album album, string[] artists = null, string[] genres = null)
	{
		albumArtist.Model = new ListStore(typeof(string), typeof(string));
		albumGenre.Model = new ListStore(typeof(string), typeof(string));

		if (album == null)
		{
			albumTitle.Text = "";
			albumDiscNumber.Value = 0;
			albumArtist.Entry.Text = "";
			albumYear.Text = "";
			albumGenre.Entry.Text = "";

			trackCount = 0;
			trackStore = new NodeStore(typeof(Track));
			trackList.NodeStore = trackStore;
		}
		else
		{
			albumTitle.Text = album.Name;
			albumDiscNumber.Value = album.Disc;
			if (artists != null)
			{
				for (int i = 0; i < artists.Length; i++)
					albumArtist.AppendText(artists[i]);
			}
			albumArtist.Entry.Text = album.Artist;
			albumYear.Text = album.Year;
			if (genres != null)
			{
				for (int i = 0; i < genres.Length; i++)
					albumGenre.AppendText(genres[i]);
			}
			albumGenre.Entry.Text = album.Genre;

			trackCount = album.Tracks.Length;
			trackStore = new NodeStore(typeof(Track));
			trackList.NodeStore = trackStore;
			for (int i = 0; i < trackCount; i++)
				trackStore.AddNode(album.Tracks[i]);
		}
	}

	public Album ExportAlbum ()
	{
		Track[] tracks = new Track[trackCount];
		for (int i = 0; i < trackCount; i++)
			tracks[i] = new Track(trackStore.GetNode(new TreePath(i.ToString())) as Track); //create new Track instance

		return new Album(albumTitle.Text, albumDiscNumber.ValueAsInt, albumArtist.Entry.Text, albumYear.Text, albumGenre.Entry.Text, tracks);
	}

	protected void OnDeleteEvent (object sender, DeleteEventArgs a)
	{
		Rip.Stop();
		Application.Quit();
		a.RetVal = true;
	}

	protected void Refresh_Activated (object sender, EventArgs e)
	{
		if (Rip.Working)
			return;

		string[] artists, genres;
		Album album = MainClass.Refresh(out artists, out genres);
		if (album == null || album.Tracks.Length < 1)
		{
			MessageDialog dialog = new MessageDialog(this, DialogFlags.Modal, MessageType.Warning, ButtonsType.Ok, Catalog.GetString("No audio CD in the drive."));
			dialog.Run();
			dialog.Destroy();
			ImportAlbum(null);
		}
		else
			ImportAlbum(album, artists, genres);
	}

	protected void Rip_Activated (object sender, EventArgs e)
	{
		if (Rip.Working)
			Rip.Stop();
		else
		{
			Track[] refreshedTracks = MainClass.RefreshOnly();
			Album album = ExportAlbum();

			if (album.Tracks.Length < 1)
			{
				MessageDialog dialog = new MessageDialog(this, DialogFlags.Modal, MessageType.Warning, ButtonsType.Ok, Catalog.GetString("The CD must be in the drive and must be scanned."));
				dialog.Run();
				dialog.Destroy();
				return;
			}

			bool different = refreshedTracks == null || refreshedTracks.Length != album.Tracks.Length;
			if (!different)
			{
				for (int i = 0; i < album.Tracks.Length; i++)
				{
					if (album.Tracks[i].Sectors != refreshedTracks[i].Sectors)
					{
						different = true;
						break;
					}
				}
			}

			if (different)
			{
				MessageDialog dialog = new MessageDialog(this, DialogFlags.Modal, MessageType.Warning, ButtonsType.Ok, Catalog.GetString("The loaded CD is different than in the drive."));
				dialog.Run();
				dialog.Destroy();
				return;
			}
			if (string.IsNullOrWhiteSpace(album.Name))
			{
				MessageDialog dialog = new MessageDialog(this, DialogFlags.Modal, MessageType.Warning, ButtonsType.Ok, Catalog.GetString("Album title cannot be blank."));
				dialog.Run();
				dialog.Destroy();
				return;
			}
			int year;
			if (!string.IsNullOrWhiteSpace(album.Year) && (!int.TryParse(album.Year, out year) || year < 1 || year > 9999))
			{
				MessageDialog dialog = new MessageDialog(this, DialogFlags.Modal, MessageType.Warning, ButtonsType.Ok, Catalog.GetString("Album year must be blank or number from 1 to 9999."));
				dialog.Run();
				dialog.Destroy();
				return;
			}

			for (int i = 0; i < album.Tracks.Length; i++)
			{
				if (string.IsNullOrWhiteSpace(album.Tracks[i].Name))
				{
					string dialogtext = string.Format(Catalog.GetString("Track {0} name cannot be blank."), (i + 1).ToString("D2"));
					MessageDialog dialog = new MessageDialog(this, DialogFlags.Modal, MessageType.Warning, ButtonsType.Ok, dialogtext);
					dialog.Run();
					dialog.Destroy();
					return;
				}
			}

			albumTitle.Sensitive = false;
			albumArtist.Sensitive = false;
			albumYear.Sensitive = false;
			albumGenre.Sensitive = false;
			nameRenderer.Editable = false;
			activeRenderer.Activatable = false;
			cdromAction.StockId = "gtk-stop";
			cdromAction.Tooltip = Catalog.GetString("Stop");

			Rip.RipAlbum(album);
		}
	}

	public void Rip_Ended ()
	{
		cdromAction.StockId = "gtk-cdrom";
		cdromAction.Tooltip = Catalog.GetString("Rip");
		albumTitle.Sensitive = true;
		albumArtist.Sensitive = true;
		albumYear.Sensitive = true;
		albumGenre.Sensitive = true;
		nameRenderer.Editable = true;
		activeRenderer.Activatable = true;
		trackList.QueueDraw();
	}

	protected void Setting_Activated (object sender, EventArgs e)
	{
		CDAR.Settings win = new CDAR.Settings();
		win.Show();
	}

	public void UpdateProgress (int track, float progress, string prefix = null)
	{
		Track node = trackStore.GetNode(new TreePath((track - 1).ToString())) as Track;
		node.Progress = progress > 100 ? 100 : progress;
		if (prefix != null)
			node.ProgressPrefix = prefix;
		trackList.QueueDraw();
	}

	public void CompleteProgress (int track, string prefix = null)
	{
		Track node = trackStore.GetNode(new TreePath((track - 1).ToString())) as Track;
		if (prefix != null)
			node.ProgressPrefix = prefix;
		else
			node.ProgressPrefix = Catalog.GetString("Done");
		node.Progress = 100;
		node.Active = false;
		trackList.QueueDraw();
	}
}