# CDAR
CDAR (Compact Disc Audio Ripper) is software for easy rip audio CD via GTK GUI.

## Requirements
This packages must be installed to run the application correctly: `mono-runtime`, `gtk-sharp2`, `cdparanoia`, `eject`.
CDAR uses this NuGet packages: `Newtonsoft.Json`, `MetaBrainz.MusicBrainz` and `MetaBrainz.MusicBrainz.DiscId`.

To convert sound, the package for the selected type must be installed: FLAC - `flac`, OGG (vorbis) - `vorbis-tools`, MP3 - `lame`, AAC - `faac`.

Packages have own license, accept them before use.

## Translation
For get .po file run `./mpo -g language`, for update `./mpo -u`

Translation files is automatic created in Gitlab CI, for manual creation run `./mpo -e output-dir`
