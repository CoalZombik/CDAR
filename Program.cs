﻿using System;
using System.Diagnostics;
using System.IO;
using Gtk;
using Mono.Unix;
using MetaBrainz.MusicBrainz;
using MetaBrainz.MusicBrainz.DiscId;
using MetaBrainz.MusicBrainz.Interfaces;
using MetaBrainz.MusicBrainz.Interfaces.Entities;
using System.Text;
using System.Reflection;

namespace CDAR
{
	static class MainClass
	{
		public const string USER_AGENT = "CDAR/1.2 ( https://gitlab.com/CoalZombik/CDAR/ )";
		public static readonly string AppData = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "CDAR");
		public static readonly string ExeDir = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);

		public static MainWindow Win;

		public static void Main ()
		{
			string localedir = Path.Combine(ExeDir, "locale");
			if (Directory.Exists(localedir))
				Catalog.Init("cdar", localedir);

			Application.Init();
			Win = new MainWindow();
			Win.Show();

			try
			{
				if (!Directory.Exists(AppData))
					Directory.CreateDirectory(AppData);
			}
			catch
			{
				string dialogtext = string.Format(Catalog.GetString("Application data directory cannot be created: <i>{0}</i>\n" +
																	"CDAR may have problems with some operations."), AppData);
				MessageDialog dialog = new MessageDialog(null, DialogFlags.Modal, MessageType.Error, ButtonsType.Ok, true, dialogtext);
				dialog.Run();
				dialog.Destroy();
			}

			Application.Run();
		}

		public static Track[] RefreshOnly ()
		{
			try
			{
				TableOfContents data = TableOfContents.ReadDisc(Config.Data.Device, DiscReadFeature.None);
				return DefaultTracks(data.Tracks);
			}
			catch
			{
				return null;
			}
		}

		public static Album Refresh (out string[] artists, out string[] genres)
		{
			artists = null;
			genres = null;

			if (!Config.Data.MusicBrainzEnable)
				return new Album(RefreshOnly());

			TableOfContents data;
			try
			{
				data = TableOfContents.ReadDisc(Config.Data.Device, DiscReadFeature.None);
			}
			catch
			{
				return null;
			}

			Query query = new Query(USER_AGENT);
			try
			{
				IDiscIdLookupResult lookup = query.LookupDiscId(data.DiscId);
				Track[] tracks = DefaultTracks(data.Tracks);

				if (lookup.Disc != null && lookup.Disc.Releases.Count > 0)
				{
					int releasesCount = lookup.Disc.Releases.Count;
					if (releasesCount > 1)
					{
						Guid[] ids = new Guid[releasesCount];
						string[] info = new string[releasesCount];
						for (int i = 0; i < releasesCount; i++)
						{
							IRelease release = lookup.Disc.Releases[i];
							info[i] = string.Format("{0}: {1} ({2})", i, release.Title, release.Date);
							ids[i] = release.MbId;
						}

						Conflict conflict = new Conflict(query, data, ids, info);
						Album album;
						if (conflict.Run() == (int)ResponseType.Ok)
						{
							artists = conflict.SelectedArtists;
							genres = conflict.SelectedGenres;
							album = conflict.SelectedAlbum;
						}
						else
							album = new Album(DefaultTracks(data.Tracks));
						conflict.Destroy();
						return album;
					}

					return GetRelease(query, data, lookup.Disc.Releases[0].MbId, out artists, out genres);
				}

				string albumName = lookup.Stub.Title;
				string artist = lookup.Stub.Artist;
				artists = new string[] { artist };
				int trackCount = lookup.Stub.Tracks.Count;
				for (int i = 0; i < trackCount; i++)
					tracks[i].Name = lookup.Stub.Tracks[i].Title;

				return new Album(albumName, 0, artist, "", "", tracks);
			}
			catch
			{
				return new Album(DefaultTracks(data.Tracks));
			}
		}

		public static Album GetRelease (Query query, TableOfContents data, Guid release, out string[] artists, out string[] genres)
		{
			artists = null;
			genres = null;
			try
			{
				IRelease releaseLookup = query.LookupRelease(release, Include.Artists | Include.Tags | Include.Recordings | Include.DiscIds);
				string albumName = releaseLookup.Title;
				string artist = "";
				if (releaseLookup.ArtistCredit != null && releaseLookup.ArtistCredit.Count > 0)
				{
					artist = releaseLookup.ArtistCredit[0].Artist.Name;
					artists = new string[releaseLookup.ArtistCredit.Count];
					for (int i = 0; i < artists.Length; i++)
						artists[i] = releaseLookup.ArtistCredit[i].Artist.Name;
				}

				string year = "";
				if (releaseLookup.Date != null)
				{
					year = releaseLookup.Date.NearestDate.Year.ToString();
				}

				string genre = "";
				if (releaseLookup.Tags != null && releaseLookup.Tags.Count > 0)
				{
					genres = new string[releaseLookup.Tags.Count];
					int maxVote = -1;
					for (int i = 0; i < genres.Length; i++)
					{
						ITag tag = releaseLookup.Tags[i];
						genres[i] = FirstLetterToUpper(tag.Name);
						if (tag.VoteCount > maxVote)
						{
							genre = genres[i];
							maxVote = tag.VoteCount;
						}
					}
				}

				int discnumber = 0;
				Track[] tracks = DefaultTracks(data.Tracks);
				IMedium medium = GetMedium(releaseLookup, data.DiscId);
				if (medium != null)
				{
					if (releaseLookup.Media.Count > 1)
						discnumber = medium.Position;
					for (int i = 0; i < tracks.Length; i++)
					{
						string trackName = medium.Tracks[i].Title;
						tracks[i].Name = trackName;
					}
				}

				return new Album(albumName, discnumber, artist, year, genre, tracks);
			}
			catch
			{
				return null;
			}
		}

		static IMedium GetMedium (IRelease release, string discid)
		{
			int mediaCount = release.Media.Count;
			for (int im = 0; im < mediaCount; im++)
			{
				IMedium medium = release.Media[im];
				int discsCount = medium.Discs.Count;
				for (int id = 0; id < discsCount; id++)
				{
					IDisc disc = medium.Discs[id];
					if (disc.Id == discid)
						return medium;
				}
			}
			return null;
		}

		static Track[] DefaultTracks (TableOfContents.AudioTrackCollection collection)
		{
			Track[] tracks = new Track[collection.Count];
			string trackText = Catalog.GetString("Track {0}");
			int i = 0;
			foreach (TableOfContents.AudioTrack track in collection)
			{
				tracks[i] = new Track(track.Number, string.Format(trackText, track.Number.ToString("D2")), track.Length);
				i++;
			}
			return tracks;
		}

		//https://stackoverflow.com/questions/4135317/make-first-letter-of-a-string-upper-case-with-maximum-performance/4135491#4135491
		public static string FirstLetterToUpper (string str)
		{
			if (str == null)
				return null;

			if (str.Length > 1)
				return char.ToUpper(str[0]) + str.Substring(1);

			return str.ToUpper();
		}

		public static bool CheckCommand (string command)
		{
			ProcessStartInfo info = new ProcessStartInfo("bash", "-c \"command -v " + command + "\"");
			info.CreateNoWindow = true;
			info.UseShellExecute = false;
			info.RedirectStandardOutput = true;
			Process test = Process.Start(info);
			test.WaitForExit();
			return test.ExitCode == 0;
		}

		//modified from https://iandevlin.com/blog/2010/01/csharp/generating-a-relative-path-in-csharp/
		public static string RelativePath (string absPath, string relTo)
		{
			string[] absDirs = absPath.Split(Path.DirectorySeparatorChar);
			string[] relDirs = relTo.Split(Path.DirectorySeparatorChar);

			// Get the shortest of the two paths
			int len = absDirs.Length < relDirs.Length ? absDirs.Length :
			relDirs.Length;

			// Use to determine where in the loop we exited
			int lastCommonRoot = -1;
			int index;

			// Find common root
			for (index = 0; index < len; index++)
			{
				if (absDirs[index] == relDirs[index]) lastCommonRoot = index;
				else break;
			}

			// If we didn't find a common prefix then throw
			if (lastCommonRoot == -1)
			{
				throw new ArgumentException("Paths do not have a common base");
			}

			// Build up the relative path
			StringBuilder relativePath = new StringBuilder();

			// Add on the ..
			for (index = lastCommonRoot + 1; index < absDirs.Length; index++)
			{
				if (absDirs[index].Length > 0) relativePath.Append(".." + Path.DirectorySeparatorChar);
			}

			// Add on the folders
			for (index = lastCommonRoot + 1; index < relDirs.Length - 1; index++)
			{
				relativePath.Append(relDirs[index] + Path.DirectorySeparatorChar);
			}
			relativePath.Append(relDirs[relDirs.Length - 1]);

			return relativePath.ToString();
		}

		public static bool Begins (this string a, string b)
		{
			if (string.IsNullOrEmpty(a) || b == null)
				return false;

			int length = b.Length;
			if (a.Length < length)
				return false;

			for (int i = 0; i < length; i++)
			{
				if (a[i] != b[i])
					return false;
			}

			return true;
		}
	}
}
