﻿using System;
using System.IO;
using Newtonsoft.Json;
using Gtk;
using Mono.Unix;

namespace CDAR
{
	public static class Config
	{
		public static readonly string Location = Path.Combine(MainClass.AppData, "config.json");

		public static ConfigData Data { get { if (data == null) Load(); return data; } }

		static ConfigData data;

		public static void Load ()
		{
			data = new ConfigData();
			if (File.Exists(Location))
			{
				try
				{
					data = JsonConvert.DeserializeObject<ConfigData>(File.ReadAllText(Location));
				}
				catch
				{
					data = new ConfigData();
				}
			}
			else
				data = new ConfigData();
		}

		public static void Save ()
		{
			if (data == null)
				data = new ConfigData();

			try
			{
				File.WriteAllText(Location, JsonConvert.SerializeObject(data, Formatting.Indented));
			}
			catch
			{
				string dialogtext = string.Format(Catalog.GetString("Configuration cannot be saved to: <i>{0}</i>."), Location);
				MessageDialog dialog = new MessageDialog(MainClass.Win, DialogFlags.DestroyWithParent, MessageType.Error, ButtonsType.Ok, true, dialogtext);
				dialog.Run();
				dialog.Destroy();
			}
		}
	}

	public class ConfigData
	{
		public string Device = "/dev/cdrom";
		public int Speed = 0; //0 = no -S argument
		public RipChecking Checking = RipChecking.Full;
		public string Filename = "%track% %title%";
		public string SingleDirectory = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyMusic), "%album%");
		public string MultiDirectory = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyMusic), "%album%", "CD %disc%");
		public bool Eject = false;

		public bool WavEnable = true;

		public bool FlacEnable = false;
		public int FlacCompress = 5; //(fast) 0-8 (best)

		public bool VorbisEnable = false;
		public int VorbisQuality = 3; //(worst) -1-10 (best)

		public bool Mp3Enable = false;
		public int Mp3Quality = 4; //(worst) 9-0 (best)

		public bool AacEnable = false;
		public float AacQuality = 1; //(worst) 0.1-5.0 (best)

		public bool MusicBrainzEnable = true;

		public bool PlaylistCreate = true;
		public string PlaylistDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyMusic);
		public string PlaylistFilename = "%album% CD %disc%";
		public bool PlaylistRelative = true;
	}

	public enum RipChecking
	{
		Full = 0,
		Limited,
		None
	}
}